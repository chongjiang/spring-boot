package com.mgy.example.web.controller.notepad;


import com.mgy.example.domain.common.WebResponse;
import com.mgy.example.domain.notepad.Notepad;
import com.mgy.example.domain.stock.TagAndNotepad;
import com.mgy.example.domain.stock.UserAndNotepad;
import com.mgy.example.service.notepad.NotepadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/notepad")
@RestController
public class NotepadController {
    @Autowired
    private NotepadService notepadService;

    @RequestMapping("/add")
    public Object add(Notepad notepad) {
        notepadService.add(notepad);
        return "添加成功";
    }

    @RequestMapping("/getByKey")
    public Object getByKey(Integer id) {
        return notepadService.getOne(id);
    }

    @RequestMapping("/getList")
    public WebResponse getList() {
        List<UserAndNotepad> list = notepadService.getUserAndNotepad(1L);
        return new WebResponse(list);
    }

    @RequestMapping("/getList2")
    public WebResponse getList2() {
        List<TagAndNotepad> list = notepadService.getTagAndNotepad(1L);
        return new WebResponse(list);
    }
}
