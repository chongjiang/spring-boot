package com.mgy.example.web.controller.user;

import com.mgy.example.domain.common.WebResponse;
import com.mgy.example.domain.stock.TagAndNotepad;
import com.mgy.example.domain.user.User;
import com.mgy.example.service.notepad.NotepadService;
import com.mgy.example.service.user.UserService;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/user")
@RestController
public class UserController {
    private final static Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;
    @Autowired
    private NotepadService notepadService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    @RequestMapping("/getUserInfo")
    public Object getUserInfo() throws SQLException {
        User user = userService.getUserInfo(1);
        logger.info("getUserInfo调用成功");
        return user;
    }

    @RequestMapping("getUser")
    public ModelAndView getUser(Integer id) throws SQLException {
        Assert.notNull(id, "id不能为空");
        User user = userService.getUserInfo(id);
        Map<String, Object> result = new HashMap<>(1);
        result.put("user", user);
        return new ModelAndView("/user/user", result);
    }

    @RequestMapping("/add")
    public WebResponse add(User user) throws SQLException {
        user.setName("李四");
        user.setAge(20);
        user.setCreated(new Date());
        user.setModified(new Date());
        userService.add(user);
        return new WebResponse();
    }


    @RequestMapping("/add2")
    public WebResponse add2(User user) throws SQLException {
        user.setName("ceshi22");
        user.setAge(22);
        user.setCreated(new Date());
        user.setModified(new Date());
        userService.add(user, 1);
        return new WebResponse();
    }

    @RequestMapping("/add3")
    public WebResponse add3() throws SQLException {
        Map<String, Object> user = new HashMap<>();
        user.put("name", "ddddd");
        user.put("age", 23);
        user.put("created", new Date());
        user.put("modified", new Date());
        userService.add(user, 1);
        return new WebResponse();
    }


    @RequestMapping("/getTest")
    public Object getTest() throws SQLException, InterruptedException, IOException {
        User user = userService.getUserInfo(1);
        logger.info("getUserInfo调用成功");
        List<TagAndNotepad> tagAndNotepads = notepadService.getTagAndNotepad(1L);
        this.test22();

        stringRedisTemplate.opsForValue().set("aa", "bb");
        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        map.put("tagAndNotepads", tagAndNotepads);
        map.put("http", test33());

        return map;
    }

    public void test22() throws InterruptedException {
        Thread.sleep(2000);
    }

    public String test33() throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("https://m.baidu.com/?from=844b&vit=fps");
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).build();
        httpGet.setConfig(requestConfig);
        CloseableHttpResponse response = httpClient.execute(httpGet);
        HttpEntity httpEntity = response.getEntity();
        return EntityUtils.toString(httpEntity, "UTF-8");
    }


}
