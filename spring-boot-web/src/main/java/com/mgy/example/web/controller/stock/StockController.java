package com.mgy.example.web.controller.stock;

import com.mgy.example.domain.common.WebResponse;
import com.mgy.example.domain.stock.Stock;
import com.mgy.example.query.stock.StockQuery;
import com.mgy.example.service.stock.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

@RequestMapping("/stock")
@RestController
public class StockController {
    @Autowired
    private StockService stockService;

    @RequestMapping("/add")
    public WebResponse add(Stock stock) throws SQLException {
        Assert.notNull(stock.getId(), "id is null");
        Assert.notNull(stock.getNum(), "num is null");
        stockService.add(stock);
        return new WebResponse();
    }

    @RequestMapping("/getOne")
    public WebResponse getOne(StockQuery stockQuery) {
        Assert.notNull(stockQuery.getId(), "id is null");
        Stock stock = stockService.getOne(stockQuery);
        return new WebResponse(stock);
    }


    @RequestMapping("/update")
    public WebResponse update(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey(stock);
        return new WebResponse();
    }

    @RequestMapping("/update1")
    public WebResponse update1(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey1(stock);
        return new WebResponse();
    }

    @RequestMapping("/update2")
    public WebResponse update2(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey2(stock);
        return new WebResponse();
    }

    @RequestMapping("/update22")
    public WebResponse update22(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey22(stock);
        return new WebResponse();
    }

    @RequestMapping("/update3")
    public WebResponse update3(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey3(stock);
        return new WebResponse();
    }


    @RequestMapping("/update4")
    public WebResponse update4(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey4(stock);
        return new WebResponse();
    }

    @RequestMapping("/update5")
    public WebResponse update5(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey5(stock);
        return new WebResponse();
    }

    @RequestMapping("/update6")
    public WebResponse update6(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey6(stock);
        return new WebResponse();
    }


    @RequestMapping("/update7")
    public WebResponse update7(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey7(stock);
        return new WebResponse();
    }

    @RequestMapping("/update8")
    public WebResponse update8(Stock stock) throws Exception {
        Assert.notNull(stock.getId(), "id is null");
        stockService.updateByKey8(stock);
        return new WebResponse();
    }

}
