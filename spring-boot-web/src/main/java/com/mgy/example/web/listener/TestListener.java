package com.mgy.example.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * 注解方式使用listener需要在启动类上加注解@ServletComponentScan
 */
@WebListener
public class TestListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("TestListener");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
