package com.mgy.example.web.listener;

import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 代码方式注入listener，不需要在启动类上加注解@ServletComponentScan
 * servletListenerRegistrationBean.setListener(new AppInitListener());
 */
public class AppInitListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("AppInitListener");

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
