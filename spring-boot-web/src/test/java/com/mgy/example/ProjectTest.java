package com.mgy.example;


import com.mgy.example.domain.TableRouter;
import com.mgy.example.utils.ParameterUtil;
import io.shardingsphere.core.rule.TableRule;
import org.apache.commons.lang3.ClassUtils;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectTest {
    public static void main(String[] args) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<>(3);
        map.put("dbNo", 1);
        map.put("tableNo", 1);


        TableRouter tableRouter = new TableRouter();
        tableRouter.setLogicTable("aa");
        tableRouter.setDbNo(1);
        tableRouter.setTableNo(1);

        Object[] arr = {tableRouter};

        Map<Object, Object> map3 = new HashMap<>(3);
        map3.put( "key",arr);
        map3.put("dbNo", 1);
        map3.put("tableNo", 1);

        String logicTable = ParameterUtil.resolve(null, map3.getClass(), map3, "logicTable");
        System.out.println(logicTable);
    }


}
