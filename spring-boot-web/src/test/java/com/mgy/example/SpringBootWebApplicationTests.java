package com.mgy.example;

import com.mgy.example.constants.Constant;
import com.mgy.example.dao.mapper.notepad.INotepadDao;
import com.mgy.example.domain.notepad.Notepad;
import com.mgy.example.service.common.config.redis.RedisMessageSender;
import com.mgy.example.service.common.lock.MySqlLockService;
import com.mgy.example.service.common.lock.RedisLockService;
import com.mgy.example.service.common.lock.ZkLockService;
import com.mgy.example.service.stock.StockService;
import io.shardingsphere.api.HintManager;
import org.apache.curator.framework.CuratorFramework;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootWebApplicationTests {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private StockService stockService;
    @Autowired
    private MySqlLockService mySqlLockService;
    @Autowired
    private RedisLockService redisLockService;
    @Autowired
    private ZkLockService zkLockService;
    @Autowired
    private CuratorFramework curatorFramework;
    @Autowired
    private RedisMessageSender redisMessageSender;
    @Autowired
    private INotepadDao notepadDao;

    @Test
    public void setNXTest() {
        String script = "local  result= redis.call('setNX',KEYS[1],ARGV[1]) ;if tonumber(result)==1 then" +
                " redis.call('expire',KEYS[1],ARGV[2])" +
                " return 1;" +
                " else" +
                " return 0;" +
                " end;";
        RedisScript<Boolean> setIfAbsentScript = new DefaultRedisScript<Boolean>(script, Boolean.class);
        List<Object> keys = new ArrayList<>();
        keys.add("aa");
        Object[] args = {"bb", 50};
        Boolean result = (Boolean) redisTemplate.<Boolean>execute(setIfAbsentScript, keys, args);
        System.out.println(result);

    }

    @Test
    public void test1() throws Exception {
        redisMessageSender.sendMessage(Constant.UNLOCK_SUCCESS);

    }


    @Test
    public void test2() {

        try (HintManager hintManager = HintManager.getInstance()) {
            hintManager.addDatabaseShardingValue("notepad", 1);
            hintManager.addTableShardingValue("notepad", 0);

            Notepad notepad = notepadDao.getByKey(1);
        }
    }


}
