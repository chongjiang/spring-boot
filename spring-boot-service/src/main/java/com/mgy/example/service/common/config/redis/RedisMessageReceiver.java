package com.mgy.example.service.common.config.redis;

import com.mgy.example.constants.Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RedisMessageReceiver {
    @Autowired
    private RedisSemaphore redisSemaphore;

    /**
     * 接收消息的方法
     *
     * @param message 订阅的消息内容
     */
    public void receiveMessage(String message) {
        System.out.println("消息体：" + message);
        if (StringUtils.equals(message, Constant.UNLOCK_SUCCESS)) {
            System.out.println("许可加1");
            redisSemaphore.release();
        }
    }
}
