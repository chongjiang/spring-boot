package com.mgy.example.service.user;

import com.mgy.example.dao.mapper.user.IUserDao;
import com.mgy.example.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Map;

/**
 * 测试
 */
@Service
public class UserService {
    @Autowired
    private IUserDao userDao;

    public User getUserInfo(Integer id) throws SQLException {
        return userDao.getByKey(id);
    }

    public Integer add(User user) throws SQLException {
        return userDao.add(user);
    }

    public Integer add(User user,Integer dbNo) throws SQLException {
        return userDao.add(user,dbNo);
    }

    public Integer add(Map<String,Object> user, Integer dbNo) throws SQLException {
        return userDao.add(user,dbNo);
    }
}
