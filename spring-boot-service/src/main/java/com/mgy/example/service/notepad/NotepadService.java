package com.mgy.example.service.notepad;

import com.mgy.example.dao.mapper.notepad.INotepadDao;
import com.mgy.example.domain.TableRouter;
import com.mgy.example.domain.notepad.Notepad;
import com.mgy.example.domain.stock.TagAndNotepad;
import com.mgy.example.domain.stock.UserAndNotepad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试
 */
@Service
public class NotepadService {
    @Autowired
    INotepadDao notepadDao;

    public void add(Notepad notepad) {
        notepadDao.add(notepad);
    }

    public Notepad getOne(Integer id) {
        return notepadDao.getByKey(id, "notepad", 1, 1);
    }


    public List<UserAndNotepad> getUserAndNotepad(Long id) {
        List<TableRouter> tableRouters = new ArrayList<>();
        TableRouter stockTable = new TableRouter();
        stockTable.setLogicTable("notepad");
        stockTable.setDbNo(0);
        stockTable.setTableNo(0);
        tableRouters.add(stockTable);

        return notepadDao.getUserAndNotepad(id, tableRouters);
    }

    public List<TagAndNotepad> getTagAndNotepad(Long id) {
        List<TableRouter> tableRouters = new ArrayList<>();
        TableRouter stockTable = new TableRouter();
        stockTable.setLogicTable("notepad");
        stockTable.setDbNo(0);
        stockTable.setTableNo(1);
        tableRouters.add(stockTable);

        TableRouter stockTable2 = new TableRouter();
        stockTable2.setLogicTable("tag");
        stockTable2.setDbNo(0);
        stockTable2.setTableNo(0);
        tableRouters.add(stockTable2);

        return notepadDao.getTagAndNotepad(id, tableRouters);
    }


}
