package com.mgy.example.service.common.config.zk;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CuratorConfig {
    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public int getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(int intervalTime) {
        this.intervalTime = intervalTime;
    }

    public String getZookeeperUrl() {
        return zookeeperUrl;
    }

    public void setZookeeperUrl(String zookeeperUrl) {
        this.zookeeperUrl = zookeeperUrl;
    }

    public int getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public String getLockRoot() {
        return lockRoot;
    }

    public void setLockRoot(String lockRoot) {
        this.lockRoot = lockRoot;
    }

    /**
     * 重试次数
     */
    @Value("${curator.retryCount}")
    private int retryCount;
    /**
     * 重试间隔时间 单位毫秒
     */
    @Value("${curator.intervalTime}")
    private int intervalTime;
    /**
     * zookeeper 地址
     */
    @Value("${curator.zookeeperUrl}")
    private String zookeeperUrl;
    /**
     * session超时时间 单位毫秒
     */
    @Value("${curator.sessionTimeout}")
    private int sessionTimeout;
    /**
     * 连接超时时间 单位毫秒
     */
    @Value("${curator.connectionTimeout}")
    private int connectionTimeout;

    /**
     * 锁的根节点
     */
    @Value("${curator.lockRoot}")
    private String lockRoot;

    @Bean(initMethod = "start")
    public CuratorFramework curatorFramework() {
        return CuratorFrameworkFactory.newClient(
                zookeeperUrl,
                sessionTimeout,
                connectionTimeout,
                new RetryNTimes(retryCount, intervalTime));
    }

}
