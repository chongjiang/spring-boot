package com.mgy.example.service.common.config.redis;

import com.mgy.example.constants.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisMessageSender {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 发布消息
     *
     * @param message 消息内容
     */
    public void sendMessage(String message) {
        stringRedisTemplate.convertAndSend(Constant.LOCK_CHANNEL, message);
    }
}
