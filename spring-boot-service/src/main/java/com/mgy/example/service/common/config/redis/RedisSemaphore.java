package com.mgy.example.service.common.config.redis;

import org.springframework.stereotype.Component;

import java.util.concurrent.Semaphore;

@Component
public class RedisSemaphore {
    /**
     * 信号量
     */
    private final Semaphore semaphore = new Semaphore(0);

    public void acquire() throws InterruptedException {
        semaphore.acquire();
    }

    public void release() {
        semaphore.release();
    }
}
