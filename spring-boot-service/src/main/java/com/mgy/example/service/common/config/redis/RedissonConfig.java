package com.mgy.example.service.common.config.redis;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {

    /**
     * 名称
     */
    @Value("${spring.redis.sentinel.master}")
    private String masterName;

    /**
     * redis服务地址
     */
    @Value("${spring.redis.host}")
    private String host;

    /**
     * redis服务端口
     */
    @Value("${spring.redis.port}")
    private Integer port;

    /**
     * 密码
     */
    @Value("${spring.redis.password}")
    private String password;

    /**
     * 连接服务超时时间
     */
    @Value("${spring.redis.timeout}")
    private Integer connectTimeout;

    /**
     * 服务响应超时时间
     */
    @Value("${singleServerConfig.timeout}")
    private Integer timeout;

    /**
     * 库索引
     */
    @Value("${spring.redis.database}")
    private Integer database;

    /**
     * redis支持单机，主从，哨兵，集群等模式
     * redis.conf配置密码 requirepass
     */
    @Bean(destroyMethod = "shutdown")
    public RedissonClient redissonClient() {
        String address = String.format("redis://%s:%d", host, port);
        //支持单机，主从，哨兵，集群等模式
        Config config = new Config();
        // 哨兵模式
//        config.useSentinelServers().setMasterName(masterName).addSentinelAddress(address).setPassword(password).
//                setConnectTimeout(connectTimeout).setTimeout(timeout).setDatabase(database);
        //单机模式
        config.useSingleServer().setAddress(address).setPassword(password).setConnectTimeout(connectTimeout).
                setTimeout(timeout).setDatabase(database);
        return Redisson.create(config);
    }

}
