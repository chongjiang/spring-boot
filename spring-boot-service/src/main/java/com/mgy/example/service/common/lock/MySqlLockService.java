package com.mgy.example.service.common.lock;

import com.mgy.example.dao.mapper.lock.ILockDao;
import com.mgy.example.domain.lock.Lock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 测试
 */
@Service
public class MySqlLockService {
    @Autowired
    private ILockDao lockDao;


    /**
     * 加锁
     *
     * @param key key
     */
    public void lock(String key) {
        Lock lock = new Lock();
        lock.setKey(key);
        lockDao.lock(lock);
    }

    /**
     * 解锁
     *
     * @param key key
     */
    public void unlock(String key) {
        Lock lock = new Lock();
        lock.setKey(key);
        lockDao.unlock(lock);
    }
}
