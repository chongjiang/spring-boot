package com.mgy.example.controller;

import com.alibaba.dubbo.rpc.service.GenericService;
import com.mgy.example.api.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mgy.example.domain.common.WebResponse;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

@RequestMapping("/test")
@RestController
public class TestController {

    @Autowired
    private HelloWorldService helloWorldService;

    @Autowired
    private ServletContext servletContext;


    @RequestMapping("/say")
    public WebResponse say(String content) {
        String result = helloWorldService.say(content);
        return new WebResponse(result);
    }

//    @RequestMapping("/say2")
//    public WebResponse say2(String content) {
//        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
//        GenericService genericService = (GenericService) context.getBean("helloWorldService2");
//        String result = (String) genericService.$invoke("say2", new String[]{String.class.getName()}, new Object[]{content});
//        return new WebResponse(result);
//    }
}
