package com.mgy.example;

import org.redisson.spring.starter.RedissonAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author mgy
 */
@EnableTransactionManagement(proxyTargetClass = true)
@ServletComponentScan
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, RedissonAutoConfiguration.class})
@SpringBootApplication(excludeName = {"com.mgy.example.dao.config.db.DataSourceConfig",
        "com.mgy.example.dao.config.db.MybatisConfig",
        "com.mgy.example.dao.config.db.MybatisInterceptor"})
public class SpringBootDubboConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootDubboConsumerApplication.class, args);
        System.out.println("spring-boot-dubbo-consumer程序启动完成");
    }
}
