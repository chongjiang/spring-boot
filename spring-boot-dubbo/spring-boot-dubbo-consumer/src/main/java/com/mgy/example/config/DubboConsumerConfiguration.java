package com.mgy.example.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations = {"classpath:dubbo/spring-dubbo.xml"})
public class DubboConsumerConfiguration {
}
