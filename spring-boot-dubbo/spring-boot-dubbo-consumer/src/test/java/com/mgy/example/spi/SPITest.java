package com.mgy.example.spi;

import com.alibaba.dubbo.common.extension.ExtensionLoader;
import com.alibaba.dubbo.rpc.Protocol;
import org.junit.Test;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.regex.Pattern;

public class SPITest {

    @Test
    public void test1() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
//        Class c = Class.forName("com.mgy.example.spi.impl.EnglishHelloWorld");
//        HelloWorld hw = (HelloWorld) c.newInstance();
//        System.out.println(hw.say("你好"));

        ServiceLoader<HelloWorld> serviceLoader = ServiceLoader.load(HelloWorld.class);
        for (HelloWorld helloWorld : serviceLoader) {
            String content = helloWorld.say("你好");
            System.out.println(content);
        }
    }


    @Test
    public void test2() {
        ExtensionLoader<HelloWorld> extensionLoader = ExtensionLoader.getExtensionLoader(HelloWorld.class);
        HelloWorld chineseHelloWorld = extensionLoader.getExtension("chinese");
        System.out.println(chineseHelloWorld.say("你好"));
        HelloWorld englishHelloWorld = extensionLoader.getExtension("english");
        System.out.println(englishHelloWorld.say("你好"));
    }

    @Test
    public void test3() {
        //Protocol
        ExtensionLoader<Protocol> extensionLoader = ExtensionLoader.getExtensionLoader(Protocol.class);
        Protocol protocol = extensionLoader.getAdaptiveExtension();
        System.out.println("");
    }
}
