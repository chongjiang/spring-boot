package com.mgy.example.spi.impl;

import com.mgy.example.spi.HelloWorld;

public class EnglishHelloWorld implements HelloWorld {
    @Override
    public String say(String content) {
        return "英文：" + content;
    }
}
