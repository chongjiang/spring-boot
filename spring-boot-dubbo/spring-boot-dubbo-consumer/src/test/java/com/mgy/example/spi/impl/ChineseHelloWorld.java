package com.mgy.example.spi.impl;

import com.alibaba.dubbo.common.extension.Adaptive;
import com.mgy.example.spi.HelloWorld;


public class ChineseHelloWorld implements HelloWorld {

    public void setHelloWorld(HelloWorld helloWorld) {
        this.helloWorld = helloWorld;
    }

    private HelloWorld helloWorld;

    public ChineseHelloWorld() {

    }

    public ChineseHelloWorld(HelloWorld helloWorld) {
        this.helloWorld = helloWorld;
    }

    @Override
    public String say(String content) {
        return "中文：" + content;
    }
}
