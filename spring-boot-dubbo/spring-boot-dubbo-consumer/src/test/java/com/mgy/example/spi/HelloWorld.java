package com.mgy.example.spi;

import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.common.extension.Adaptive;
import com.alibaba.dubbo.common.extension.SPI;


@SPI
public interface HelloWorld {

    String say(String content);
}
