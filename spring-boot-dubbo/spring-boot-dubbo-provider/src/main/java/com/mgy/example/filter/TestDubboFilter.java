package com.mgy.example.filter;

import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;

/**
 * 过滤器
 * group:所属组，例如消费端、服务端
 * order:值越小，越先执行
 */
@Activate(group = Constants.PROVIDER, order = 1)
public class TestDubboFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        String clientIp = RpcContext.getContext().getRemoteHost();
        String interfaceName = invoker.getInterface().getSimpleName() + "." + invocation.getMethodName();
        System.out.println("clientIp=" + clientIp + ";interfaceName=" + interfaceName);

        return invoker.invoke(invocation);


        //return RpcResult();
    }
}
