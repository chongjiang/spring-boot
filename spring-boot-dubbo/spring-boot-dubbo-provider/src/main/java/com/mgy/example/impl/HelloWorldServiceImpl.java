package com.mgy.example.impl;

import com.mgy.example.api.HelloWorldService;
import org.springframework.stereotype.Service;

@Service("helloWorldService")
public class HelloWorldServiceImpl implements HelloWorldService {
    @Override
    public String say(String content) {
        return content;
    }
}
