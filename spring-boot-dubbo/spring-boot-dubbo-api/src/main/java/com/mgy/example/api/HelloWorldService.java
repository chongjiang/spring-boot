package com.mgy.example.api;

public interface HelloWorldService {

    String say(String content);
}
