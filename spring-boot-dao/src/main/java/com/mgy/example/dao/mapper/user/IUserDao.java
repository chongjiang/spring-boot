package com.mgy.example.dao.mapper.user;

import com.mgy.example.domain.user.User;
import com.mgy.example.query.user.UserQuery;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * 测试
 *
 * @author maguoyong
 * @since 2018/06/22
 */
public interface IUserDao {
    /**
     * 插入一条记录
     *
     * @param user
     * @return
     */
    Integer add(@Param("user") User user) throws SQLException;

    Integer add(@Param("user") User user, Integer dbNo) throws SQLException;

    Integer add(@Param("user") Map<String,Object> user,Integer dbNo) throws SQLException;

    /**
     * 根据主键删除记录
     *
     * @param id
     * @throws SQLException
     */
    void deleteByKey(Integer id) throws SQLException;

    /**
     * 根据多个主键删除
     *
     * @param keys
     * @throws SQLException
     */
    void deleteByKeys(@Param("keys") List<Integer> keys) throws SQLException;

    /**
     * 根据主键更新记录
     *
     * @param user
     * @throws SQLException
     */
    void updateByKey(User user) throws SQLException;

    /**
     * 根据主键查询记录
     *
     * @param id
     * @return
     * @throws SQLException
     */
    User getByKey(Integer id) throws SQLException;

    /**
     * 根据条件查询一条记录
     *
     * @param userQuery
     * @return
     * @throws SQLException
     */
    User getOne(UserQuery userQuery) throws SQLException;

    /**
     * 根据多个主键id查询
     *
     * @param keys
     * @return
     * @throws SQLException
     */
    List<User> getListByKeys(List<Integer> keys) throws SQLException;

    /**
     * 根据条件查询记录
     *
     * @param userQuery
     * @return
     * @throws SQLException
     */
    List<User> getList(UserQuery userQuery) throws SQLException;

    /**
     * 分页查询
     *
     * @param userQuery
     * @return
     * @throws SQLException
     */
    List<User> getListWithPage(UserQuery userQuery) throws SQLException;

    /**
     * 根据条件查询记录数
     *
     * @param userQuery
     * @return
     * @throws SQLException
     */
    int getListCount(UserQuery userQuery) throws SQLException;
}
