package com.mgy.example.dao.mapper.lock;


import com.mgy.example.domain.lock.Lock;

public interface ILockDao {

    /**
     * 加锁
     */
    void lock(Lock lock);

    /**
     * 解锁
     *
     */
    void unlock(Lock lock);

}
