package com.mgy.example.dao.config.db;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * druid数据源配置
 *
 * @author mgy
 */
@Configuration
public class DataSourceConfig {

    @Bean(name = "dataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        return new DruidDataSource();
    }

    @Bean(name = "dataSource0")
    @ConfigurationProperties(prefix = "spring.datasource0")
    public DataSource dataSource0() {
        return new DruidDataSource();
    }

    @Bean(name = "dataSource1")
    @ConfigurationProperties(prefix = "spring.datasource1")
    public DataSource dataSource1() {
        return new DruidDataSource();
    }

    @Bean(name = "dataSourceMap")
    public Map<Object, Object> dataSourceMap() {
        Map<Object, Object> dataSourceMap = new HashMap<>(6);
        dataSourceMap.put(-1, dataSource());
        dataSourceMap.put(0, dataSource0());
        dataSourceMap.put(1, dataSource1());
        return dataSourceMap;
    }
}
