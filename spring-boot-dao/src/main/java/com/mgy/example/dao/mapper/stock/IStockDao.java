package com.mgy.example.dao.mapper.stock;


import com.mgy.example.domain.stock.Stock;
import com.mgy.example.query.stock.StockQuery;
import org.apache.ibatis.annotations.Param;

import java.sql.SQLException;
import java.util.List;

public interface IStockDao {

    /**
     * 插入一条记录
     *
     * @param stock
     * @return
     */
    void add(Stock stock) throws SQLException;

    /**
     * 根据主键删除记录
     *
     * @param id
     * @throws SQLException
     */
    void deleteByKey(Long id) throws SQLException;

    /**
     * 根据多个主键删除
     *
     * @param keys
     * @throws SQLException
     */
    void deleteByKeys(@Param("keys") List<Long> keys) throws SQLException;

    /**
     * 根据主键更新记录
     *
     * @param stock
     * @throws SQLException
     */
    void updateByKey(Stock stock) throws SQLException;

    /**
     * 根据主键查询记录
     *
     * @param id
     * @return
     * @throws SQLException
     */
    Stock getByKey(Long id) throws SQLException;

    Stock getOne(StockQuery stockQuery);

}
