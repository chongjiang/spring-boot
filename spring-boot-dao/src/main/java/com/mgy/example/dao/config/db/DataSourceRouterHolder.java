package com.mgy.example.dao.config.db;

/**
 * 分库号保存线程本地变量
 *
 * @author mgy
 */
public class DataSourceRouterHolder {
    private static final ThreadLocal<Integer> contextHolder = new ThreadLocal<>();

    public static void setDbNo(Integer dbNo) {
        contextHolder.set(dbNo);
    }

    public static Integer getDbNo() {
        return contextHolder.get();
    }

    public static void removeDbNo() {
        contextHolder.remove();
    }
}
