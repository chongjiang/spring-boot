package com.mgy.example.dao.mapper.notepad;


import com.mgy.example.domain.TableRouter;
import com.mgy.example.domain.notepad.Notepad;
import com.mgy.example.domain.stock.TagAndNotepad;
import com.mgy.example.domain.stock.UserAndNotepad;
import com.mgy.example.query.notepad.NotepadQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 测试
 *
 * @author maguoyong
 * @since 2018/06/22
 */
public interface INotepadDao {

    Integer add(Notepad notepad);

    Integer updateByKey(Notepad notepad);

    Notepad getOne(NotepadQuery notepadQuery);

    Notepad getByKey(Integer id);

    Notepad getByKey(Integer id, String logicTable, Integer dbNo, Integer tableNo);

    Notepad getByKey(Integer id, List<TableRouter> tableRouters);


    List<UserAndNotepad> getUserAndNotepad(Long notepadId, List<TableRouter> tableRouters);

    List<Notepad> getNotepadList(Long notepadId, List<TableRouter> tableRouters);

    List<TagAndNotepad> getTagAndNotepad(@Param("userId") Long userId, @Param("tableRouters") List<TableRouter> tableRouters);
}
