package com.mgy.example.dao.config.db.sharding;

import com.mgy.example.constants.Constant;
import io.shardingsphere.api.algorithm.sharding.ListShardingValue;
import io.shardingsphere.api.algorithm.sharding.ShardingValue;
import io.shardingsphere.api.algorithm.sharding.hint.HintShardingAlgorithm;

import java.util.Collection;
import java.util.Collections;

/**
 * 基于hint的分表策略
 *
 * @author mgy
 */
public class TableHintShardingAlgorithm implements HintShardingAlgorithm {
    /**
     * Sharding.
     *
     * <p>sharding value injected by hint, not in SQL.</p>
     *
     * @param availableTargetNames available data sources or tables's names
     * @param shardingValue        sharding value
     * @return sharding result for data sources or tables's names
     */
    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, ShardingValue shardingValue) {
        for (String each : availableTargetNames) {
            int tableNo = (int) ((ListShardingValue) shardingValue).getValues().iterator().next();
            int eachTableNo = Integer.valueOf(each.substring(each.lastIndexOf(Constant.UNDERLINE) + 1));
            if (eachTableNo == tableNo) {
                return Collections.singletonList(each);
            }
        }
        return null;
    }
}
