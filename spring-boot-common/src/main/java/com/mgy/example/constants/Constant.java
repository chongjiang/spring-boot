package com.mgy.example.constants;

public class Constant {
    /**
     * 频道 channel
     */
    public static final String LOCK_CHANNEL = "redis_lock";

    public static final String LOCK_SUCCESS = "lock_success";

    public static final String UNLOCK_SUCCESS = "unlock_success";

    /**
     *  下划线
     */
    public static final String UNDERLINE="_";

}
