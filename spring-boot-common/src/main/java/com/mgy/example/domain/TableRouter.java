package com.mgy.example.domain;

/**
 * 表路由配置
 *
 * @author mgy
 */
public final class TableRouter {
    public String getLogicTable() {
        return logicTable;
    }

    public void setLogicTable(String logicTable) {
        this.logicTable = logicTable;
    }

    public Integer getDbNo() {
        return dbNo;
    }

    public void setDbNo(Integer dbNo) {
        this.dbNo = dbNo;
    }

    public Integer getTableNo() {
        return tableNo;
    }

    public void setTableNo(Integer tableNo) {
        this.tableNo = tableNo;
    }

    /**
     * 逻辑表名
     */
    private String logicTable;

    /**
     * 分库号
     */
    private Integer dbNo;

    /**
     * 分表号
     */
    private Integer tableNo;
}
