package com.mgy.example.domain.common;

import java.io.Serializable;

public class WebResponse implements Serializable {
    private int result = 100;

    private String message = "success!";

    private String apiName;

    private Object data;

    public WebResponse() {

    }

    public WebResponse(Object data) {
        this.data = data;
    }

    public WebResponse(String message, String apiName, Object data) {
        this.message = message;
        this.apiName = apiName;
        this.data = data;
    }

    public WebResponse(String message, Object data) {
        this.message = message;
        this.data = data;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }
}
