package com.mgy.example.annotation;

import java.lang.annotation.*;

/**
 * 忽略暗示分片
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HintIgnore {
}
