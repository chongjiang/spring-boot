package com.mgy.example.annotation;

import java.lang.annotation.*;

/**
 * 分布式锁注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisLock {
    /**
     * 锁的名称
     */
    String lockName() default "";

    /**
     * 获取锁等待超时时间，默认值为0，表示永久等待，直到获取锁，单位秒
     */
    long waitTime() default 0L;

    /**
     * 锁超时时间，默认值为0，表示永久不超时，直到显示释放锁，单位秒
     */
    long timeout() default 0L;

    /**
     * 字段名字，指定字段值作为锁的名字,如果lockName为空，此值才有效
     */
    String fieldName() default "";

}
