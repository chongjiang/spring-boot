package com.mgy.example.annotation;

import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Transactional(rollbackFor = Exception.class)
public @interface MySqlLock {
    /**
     * 锁的名称
     */
    String lockName() default "";

    /**
     * 字段名字，指定字段值作为锁的名字,如果lockName为空，此值才有效
     */
    String fieldName() default "";
}
