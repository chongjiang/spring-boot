package com.mgy.example.query.notepad;


import com.mgy.example.query.BaseQuery;
import com.mgy.example.query.notepad.NotepadQuery;
import java.util.Date;

/**
 * 测试
 *
 * @author maguoyong
 * @since 2018/06/22
 */
public class NotepadQuery extends BaseQuery {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    private Integer id;
    private Integer userId;
    private String title;
    private String content;
    private String tag;
    private Date created;
    private Date modified;
}
