package com.mgy.example.query.stock;

import java.util.Date;
import java.util.List;

/**
 * Created by john on 2018/6/20.
 */
public class StockQuery {


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }


    public List<Long> getKeys() {
        return keys;
    }

    public void setKeys(List<Long> keys) {
        this.keys = keys;
    }

    private Long id;

    private Integer num;

    private List<Long> keys;
}
