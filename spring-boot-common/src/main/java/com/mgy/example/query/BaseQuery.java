package com.mgy.example.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 测试
 *
 * @author maguoyong
 * @since 2018/06/22
 */
public abstract class BaseQuery implements Serializable {
    /**
     * 默认每页大小
     */
    public final static int DEFAULT_PAGE_SIZE = 50;
    /**
     * 默认第几页
     */
    public final static int DEFAULT_PAGE_INDEX = 1;
    /**
     * 每页大小
     */
    protected Integer pageSize = DEFAULT_PAGE_SIZE;
    /**
     * 起始行
     */
    protected Integer startRow;
    /**
     * 结束行
     */
    protected Integer endRow;
    protected Integer pageIndex = DEFAULT_PAGE_INDEX;

    /**
     * 分表标识
     */
    protected String tableName;

    /**
     * 分库标识
     */
    private String fkId;

    public Integer getStartRow() {
        return startRow;
    }

    public BaseQuery setStartRow(Integer startRow) {
        this.startRow = startRow;
        return this;
    }

    public Integer getEndRow() {
        return endRow;
    }

    public BaseQuery setEndRow(Integer endRow) {
        this.endRow = endRow;
        return this;
    }

    public BaseQuery setPageIndex(Integer pageIndex) {
        if (pageIndex == null || pageIndex <= 0) {
            pageIndex = DEFAULT_PAGE_INDEX;
        }
        this.pageIndex = pageIndex;
        this.startRow = (pageIndex - 1) * this.pageSize;
        this.endRow = this.startRow + this.pageSize - 1;
        return this;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public BaseQuery setPageSize(Integer pageSize) {
        if (pageSize == null || pageSize < 1) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        this.pageSize = pageSize;
        this.startRow = (pageIndex - 1) * this.pageSize;
        this.endRow = this.startRow + this.pageSize - 1;
        return this;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    /**
     * 查询的字段
     */
    private String fields = "";

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFkId() {
        return fkId;
    }

    public void setFkId(String fkId) {
        this.fkId = fkId;
    }


    /***********排序***********/
    private List<OrderField> orderFields = new ArrayList<>();

    public void orderById(boolean isAsc) {
        orderFields.add(new OrderField("id", isAsc ? "ASC" : "DESC"));
    }

    public void orderByName(boolean isAsc) {
        orderFields.add(new OrderField("name", isAsc ? "ASC" : "DESC"));
    }

    public void orderByAge(boolean isAsc) {
        orderFields.add(new OrderField("age", isAsc ? "ASC" : "DESC"));
    }

    public void orderBySex(boolean isAsc) {
        orderFields.add(new OrderField("sex", isAsc ? "ASC" : "DESC"));
    }

    public class OrderField {
        public OrderField(String fieldName, String order) {
            super();
            this.fieldName = fieldName;
            this.order = order;
        }

        private String fieldName;
        private String order;

        public String getFieldName() {
            return fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }

    }

}
