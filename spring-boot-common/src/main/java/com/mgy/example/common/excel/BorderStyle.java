package com.mgy.example.common.excel;

/**
 * 单元格边框样式
 * @author maguoyong
 * @since 2018/01/24
 */
public interface BorderStyle {
 short NONE = 0;
 short THIN = 1;
 short MEDIUM = 2;
 short DASHED = 3;
 short DOTTED = 4;
 short THICK = 5;
 short DOUBLE = 6;
 short HAIR = 7;
 short MEDIUM_DASHED = 8;
 short DASH_DOT = 9;
 short MEDIUM_DASH_DOT = 10;
 short DASH_DOT_DOT = 11;
 short MEDIUM_DASH_DOT_DOT = 12;
 short SLANTED_DASH_DOT = 13;
}
