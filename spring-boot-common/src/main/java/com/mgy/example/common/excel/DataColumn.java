package com.mgy.example.common.excel;

/**
 * 列定义
 * @author maguoyong
 * @since 2018/01/24
 */
public class DataColumn {
    /**
     * 表
     */
    private DataTable table;

    /**
     * 列名
     */
    private String columnName;
    /**
     * 列索引
     */
    private int ordinal;
    /**
     * 列数据类型
     */
    private Class dataType;

    public DataColumn() {
        this("default1");
    }

    public DataColumn(Class dataType) {
        this("default1", dataType);
    }

    public DataColumn(String columnName) {
        this(columnName, String.class);
    }

    public DataColumn(String columnName, Class dataType) {
        this.setDataType(dataType);
        this.columnName = columnName;
    }

    public String getColumnName() {
        return this.columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public DataTable getTable() {
        return this.table;
    }

    public void setTable(DataTable table) {
        this.table = table;
    }


    public void setDataType(Class dataType) {
        this.dataType = dataType;
    }


    public Class getDataType() {
        return dataType;
    }


    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }


    public int getOrdinal() {
        return ordinal;
    }

    /**
     * 将输入数据转为当前列的数据类型返回
     */
    public Object convertTo(Object value) {
        return value;
    }

    @Override
    public String toString() {
        return this.columnName;
    }

}
