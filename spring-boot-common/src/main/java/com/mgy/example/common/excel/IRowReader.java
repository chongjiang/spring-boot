package com.mgy.example.common.excel;

import java.util.List;

/**
 * 行处理
 *
 * @author maguoyong
 * @date 2019.06.23
 */
public interface IRowReader {

    /**
     * 行
     *
     * @param sheetIndex 工作簿索引
     * @param sheetName  工作簿名称
     * @param rowIndex   行索引
     * @param row        行数据
     */
    void rowRead(int sheetIndex, String sheetName, int rowIndex, List<String> row);
}
