package com.mgy.example.common.excel;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.eventusermodel.*;
import org.apache.poi.hssf.eventusermodel.dummyrecord.LastCellOfRowDummyRecord;
import org.apache.poi.hssf.eventusermodel.dummyrecord.MissingCellDummyRecord;
import org.apache.poi.hssf.record.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 支持excel2003及以下版本
 * 事件模式，支持较大excel的读取操作，占用内存小
 *
 * @author maguoyong
 * @date 2019.06.23
 */
public class Excel2003Reader implements HSSFListener {
    private POIFSFileSystem poifsFileSystem;
    private FormatTrackingHSSFListener formatListener;
    /**
     * sheet索引
     */
    private int sheetIndex = -1;
    /**
     * 当前读取的工作簿名称
     */
    private String sheetName = null;

    /**
     * 当前工作簿的索引
     */
    private int currentSheetIndex = -1;
    /**
     * sheet索引和sheet名称的映射
     */
    private Map<Integer, String> sheetNameMap = new HashMap<>();

    /**
     * SSTRecord中存储了在Excel中文本单元格中的文本值，文本单元格通过索引获取文本值
     */
    private SSTRecord sstRecord;

    /**
     * 当前sheet数据
     */
    private DataTable dataTable;
    /**
     * 所有sheet数据
     */
    private List<DataTable> dataTables = new ArrayList<>();
    /**
     * 当前sheet的行数
     */
    private int rowNumber = 0;
    /**
     * 当前sheet的列数 Column
     */
    private int columnNumber = 0;

    /**
     * 当前单元格所在的行
     */
    private int currentCellRow = -1;
    /**
     * 当前单元格所在的列
     */
    private int currentCellColumn = -1;
    /**
     * 空列头的值
     */
    private String emptyColumnHeader = "";

    /**
     * 当前行的内容
     */
    private List<String> currentRow = new ArrayList<>();

    /**
     * 指定读取的sheet名称
     */
    private String readSheetName = null;
    /**
     * 指定读取的sheet索引
     */
    private int readSheetIndex = -1;

    private IRowReader rowReader;


    private Excel2003Reader(InputStream inputStream, String sheetName, IRowReader rowReader) throws IOException {
        this.readSheetName = sheetName;
        this.rowReader = rowReader;
        init(inputStream);
    }

    private Excel2003Reader(InputStream inputStream, int sheetIndex, IRowReader rowReader) throws IOException {
        this.readSheetIndex = sheetIndex;
        this.rowReader = rowReader;
        init(inputStream);
    }


    private void init(InputStream inputStream) throws IOException {
        try {
            poifsFileSystem = new POIFSFileSystem(inputStream);
            MissingRecordAwareHSSFListener listener = new MissingRecordAwareHSSFListener(this);
            formatListener = new FormatTrackingHSSFListener(listener);
            //创建事件工厂
            HSSFEventFactory factory = new HSSFEventFactory();
            HSSFRequest request = new HSSFRequest();
            //为所有的record注册一个监听器
            request.addListenerForAllRecords(formatListener);
            // 根据WorkBook输入流处理所有事件
            factory.processWorkbookEvents(request, poifsFileSystem);
        } finally {
            if (poifsFileSystem != null) {
                poifsFileSystem.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    /**
     * 指定读取的sheet是否存在
     *
     * @param sheetName sheet的名称
     * @return true:存在，false:不存在
     */
    private Boolean isExistSheet(String sheetName) {
        return sheetNameMap.values().contains(sheetName);
    }

    /**
     * 指定读取的sheet是否存在
     *
     * @param sheetIndex sheet的索引
     * @return true:存在，false:不存在
     */
    private Boolean isExistSheet(int sheetIndex) {
        return sheetNameMap.keySet().contains(sheetIndex);
    }


    @Override
    public void processRecord(Record record) {
        String currentCellValue = null;
        try {
            switch (record.getSid()) {
                //HSSFWorkbook、HSSFSheet的开始
                case BOFRecord.sid:
                    BOFRecord bofRecord = (BOFRecord) record;
                    if (bofRecord.getType() == BOFRecord.TYPE_WORKSHEET) {
                        currentCellRow = -1;
                        currentCellColumn = -1;
                        emptyColumnHeader = "";
                        //当前sheet的索引
                        currentSheetIndex++;
                        sheetName = sheetNameMap.get(currentSheetIndex);
                        if (readSheetName != null) {
                            Boolean isExistSheet = isExistSheet(readSheetName);
                            if (!isExistSheet) {
                                throw new Exception("sheet不存在,sheetName=" + readSheetName);
                            }
                            if (!StringUtils.equals(sheetName, readSheetName)) {
                                return;
                            }
                        }
                        if (readSheetIndex > -1) {
                            if (readSheetIndex != currentSheetIndex) {
                                return;
                            }
                            Boolean isExistSheet = isExistSheet(readSheetIndex);
                            if (!isExistSheet) {
                                throw new Exception("sheet不存在,sheetIndex=" + readSheetIndex);
                            }
                        }
                        if (rowReader == null) {
                            dataTable = new DataTable(sheetName);
                        }
                    }
                    break;
                //HSSFWorkbook、HSSFSheet的结束
                case EOFRecord.sid:
                    if (dataTable != null) {
                        dataTables.add(dataTable);
                    }
                    break;
                //记录了sheetName,读取sheet内容之前，会遍历所有的sheet
                case BoundSheetRecord.sid:
                    BoundSheetRecord boundSheetRecord = (BoundSheetRecord) record;
                    sheetIndex++;
                    sheetNameMap.put(sheetIndex, boundSheetRecord.getSheetname());
                    break;
                //记录了每个Sheet的有效起始结束行列索引
                case DimensionsRecord.sid:
                    DimensionsRecord dimensionsRecord = (DimensionsRecord) record;
                    rowNumber = dimensionsRecord.getLastRow() - dimensionsRecord.getFirstRow();
                    columnNumber = dimensionsRecord.getLastCol() - dimensionsRecord.getFirstCol();
                    break;
                //记录了每个Sheet中的合并单元格信息
                case MergeCellsRecord.sid:
                    MergeCellsRecord mergeCellsRecord = (MergeCellsRecord) record;
                    System.out.println("mergeCellsRecord");
                    break;
                //Sheet中空单元格，存在单元格样式
                case BlankRecord.sid:
                    BlankRecord blankRecord = (BlankRecord) record;
                    currentCellRow = blankRecord.getRow();
                    currentCellColumn = blankRecord.getColumn();
                    break;
                // Sheet中布尔或错误单元格
                case BoolErrRecord.sid:
                    BoolErrRecord boolErrRecord = (BoolErrRecord) record;
                    currentCellRow = boolErrRecord.getRow();
                    currentCellColumn = boolErrRecord.getColumn();
                    currentCellValue = boolErrRecord.getBooleanValue() + "";
                    break;
                //记录了所有Sheet的文本单元格的文本
                case SSTRecord.sid:
                    sstRecord = (SSTRecord) record;
                    break;
                // 单元格为字符串类型
                case LabelSSTRecord.sid:
                    LabelSSTRecord labelSSTRecord = (LabelSSTRecord) record;
                    currentCellRow = labelSSTRecord.getRow();
                    currentCellColumn = labelSSTRecord.getColumn();
                    if (sstRecord == null) {
                        currentCellValue = "";
                    } else {
                        currentCellValue = sstRecord.getString(labelSSTRecord.getSSTIndex()).toString();
                    }
                    break;
                //只读，支持读取直接存储在单元格中的字符串，而不是存储在SSTRecord中，除了读取不要使用LabelRecord，应该使用SSTRecord替代
                case LabelRecord.sid:
                    LabelRecord labelRecord = (LabelRecord) record;
                    currentCellRow = labelRecord.getRow();
                    currentCellColumn = labelRecord.getColumn();
                    currentCellValue = labelRecord.getValue();
                    break;
                // 单元格为数字类型
                case NumberRecord.sid:
                    NumberRecord numberRecord = (NumberRecord) record;
                    currentCellRow = numberRecord.getRow();
                    currentCellColumn = numberRecord.getColumn();
                    currentCellValue = formatListener.formatNumberDateCell(numberRecord);
                    break;
                default:
                    break;
            }
            // 单元格空值的操作
            if (record instanceof MissingCellDummyRecord) {
                MissingCellDummyRecord missingCellDummyRecord = (MissingCellDummyRecord) record;
                currentCellRow = missingCellDummyRecord.getRow();
                currentCellColumn = missingCellDummyRecord.getColumn();
                currentCellValue = "";
            }

            if (readSheetIndex > -1 && readSheetIndex != currentSheetIndex) {
                return;
            }
            if (readSheetName != null && !StringUtils.equals(readSheetName, sheetName)) {
                return;
            }
            // 行结束时的操作
            if (record instanceof LastCellOfRowDummyRecord) {
                while (currentCellColumn < columnNumber - 1) {
                    if (currentCellRow == 0 && rowReader == null) {
                        emptyColumnHeader += " ";
                        dataTable.addColumn(emptyColumnHeader, String.class);
                    }
                    currentRow.add("");
                    currentCellColumn++;
                }
                if (currentCellRow > 0 && rowReader == null) {
                    dataTable.addRow(currentRow.toArray());
                }
                if (rowReader != null) {
                    rowReader.rowRead(currentSheetIndex, sheetName, currentCellRow, currentRow);
                }
                currentRow.clear();
            } else {
                //默认认为第一行为列头
                if (currentCellRow == 0 && rowReader == null) {
                    if (StringUtils.isEmpty(currentCellValue)) {
                        emptyColumnHeader += " ";
                        dataTable.addColumn(emptyColumnHeader, String.class);
                    } else {
                        dataTable.addColumn(currentCellValue, String.class);
                    }
                }
                if (currentCellRow >= 0) {
                    currentRow.add(currentCellValue);
                }
            }
        } catch (Exception e) {
            System.out.println("发生错误：" + e.getMessage());
        }
    }


    /**
     * 读取一个excel的所有工作簿
     *
     * @param inputStream 文件流
     */
    public static List<DataTable> readAllSheet(InputStream inputStream) throws IOException {
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, null, null);
        return excel2003Reader.dataTables;
    }

    /**
     * 根据工作簿名称读取指定的工作簿
     *
     * @param inputStream 文件流
     * @param sheetName   工作簿名称
     */
    public static DataTable readSheet(InputStream inputStream, String sheetName) throws IOException {
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, sheetName, null);
        return excel2003Reader.dataTable;
    }

    /**
     * 根据工作簿索引读取指定的工作簿
     *
     * @param inputStream 文件流
     * @param sheetIndex  工作簿索引
     */
    public static DataTable readSheet(InputStream inputStream, int sheetIndex) throws IOException {
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, sheetIndex, null);
        return excel2003Reader.dataTable;
    }

    /**
     * 读取excel的第一个工作簿
     *
     * @param inputStream 文件流
     */
    public static DataTable readFirstSheet(InputStream inputStream) throws IOException {
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, 0, null);
        return excel2003Reader.dataTable;
    }

    private static InputStream getFileInputStream(File excelFile) throws FileNotFoundException {
        if (excelFile == null || !excelFile.isFile()) {
            throw new IllegalArgumentException("excel文件不存在");
        }
        return new FileInputStream(excelFile);
    }

    /**
     * 读取一个excel的所有工作簿
     *
     * @param excelFile excel文件
     */
    public static List<DataTable> readAllSheet(File excelFile) throws IOException {
        InputStream inputStream = getFileInputStream(excelFile);
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, null, null);
        return excel2003Reader.dataTables;
    }

    /**
     * 根据工作簿名称读取指定的工作簿
     *
     * @param excelFile excel文件
     * @param sheetName 工作簿名称
     */
    public static DataTable readSheet(File excelFile, String sheetName) throws IOException {
        InputStream inputStream = getFileInputStream(excelFile);
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, sheetName, null);
        return excel2003Reader.dataTable;
    }

    /**
     * 根据工作簿索引读取指定的工作簿
     *
     * @param excelFile  excel文件
     * @param sheetIndex 工作簿索引
     */
    public static DataTable readSheet(File excelFile, int sheetIndex) throws IOException {
        InputStream inputStream = getFileInputStream(excelFile);
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, sheetIndex, null);
        return excel2003Reader.dataTable;
    }

    /**
     * 读取excel的第一个工作簿
     *
     * @param excelFile excel文件
     */
    public static DataTable readFirstSheet(File excelFile) throws IOException {
        InputStream inputStream = getFileInputStream(excelFile);
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, 0, null);
        return excel2003Reader.dataTable;
    }

    /**
     * 读取一个excel的所有工作簿
     *
     * @param inputStream 文件流
     * @param rowReader   回调
     */
    public static void readAllSheet(InputStream inputStream, IRowReader rowReader) throws IOException {
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, null, rowReader);
    }

    /**
     * 根据工作簿名称读取指定的工作簿
     *
     * @param inputStream 文件流
     * @param sheetName   工作簿名称
     * @param rowReader   回调
     */
    public static void readSheet(InputStream inputStream, String sheetName, IRowReader rowReader) throws IOException {
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, sheetName, rowReader);
    }

    /**
     * 根据工作簿索引读取指定的工作簿
     *
     * @param inputStream 文件流
     * @param sheetIndex  工作簿索引
     * @param rowReader   回调
     */
    public static DataTable readSheet(InputStream inputStream, int sheetIndex, IRowReader rowReader) throws IOException {
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, sheetIndex, rowReader);
        return excel2003Reader.dataTable;
    }

    /**
     * 读取excel的第一个工作簿
     *
     * @param inputStream 文件流
     * @param rowReader   回调
     */
    public static DataTable readFirstSheet(InputStream inputStream, IRowReader rowReader) throws IOException {
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, 0, rowReader);
        return excel2003Reader.dataTable;
    }

    /**
     * 读取一个excel的所有工作簿
     *
     * @param excelFile excel文件
     * @param rowReader 回调
     */
    public static void readAllSheet(File excelFile, IRowReader rowReader) throws IOException {
        InputStream inputStream = getFileInputStream(excelFile);
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, null, rowReader);
    }

    /**
     * 根据工作簿名称读取指定的工作簿
     *
     * @param excelFile excel文件
     * @param sheetName 工作簿名称
     * @param rowReader 回调
     */
    public static void readSheet(File excelFile, String sheetName, IRowReader rowReader) throws IOException {
        InputStream inputStream = getFileInputStream(excelFile);
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, sheetName, rowReader);
    }

    /**
     * 根据工作簿索引读取指定的工作簿
     *
     * @param excelFile  excel文件
     * @param sheetIndex 工作簿索引
     * @param rowReader  回调
     */
    public static void readSheet(File excelFile, int sheetIndex, IRowReader rowReader) throws IOException {
        InputStream inputStream = getFileInputStream(excelFile);
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, sheetIndex, rowReader);
    }

    /**
     * 读取excel的第一个工作簿
     *
     * @param excelFile excel文件
     * @param rowReader 回调
     */
    public static void readFirstSheet(File excelFile, IRowReader rowReader) throws IOException {
        InputStream inputStream = getFileInputStream(excelFile);
        Excel2003Reader excel2003Reader = new Excel2003Reader(inputStream, 0, rowReader);
    }


    public static void main2(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("/Users/gaoyuan/Downloads/含发件人EXCEL模板.xls");
        List<DataTable> tables = new ArrayList<>();
        DataTable table = Excel2003Reader.readSheet(fileInputStream, "Sheet1");
        tables.add(table);
        for (DataTable dt : tables) {
            System.out.println("表名：" + dt.getTableName());
            for (DataColumn dataColumn : dt.getColumns()) {
                System.out.print(dataColumn.getColumnName() + "\t");
            }
            System.out.println("");
            for (DataRow dr : dt.getRows()) {
                for (DataColumn dataColumn : dt.getColumns()) {
                    String value = (String) dr.getValue(dataColumn.getOrdinal());
                    String value1 = StringUtils.isBlank(value) ? "空" : value;
                    System.out.print(value1 + "\t");
                }
                System.out.println("");
            }
            System.out.println("------------");
        }

    }

    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("/Users/gaoyuan/Downloads/含发件人EXCEL模板.xls");
        Excel2003Reader.readSheet(fileInputStream, "Sheet1", (int sheetIndex, String sheetName, int rowIndex, List<String> row) -> {
            System.out.println("sheetIndex=" + sheetIndex + ";sheetName=" + sheetName + ";rowIndex=" + rowIndex);
            for (String rowInfo : row) {
                rowInfo = StringUtils.isEmpty(rowInfo) ? "空" : rowInfo;
                System.out.print(rowInfo + "\t");
            }
            System.out.println("");
        });
    }
}
