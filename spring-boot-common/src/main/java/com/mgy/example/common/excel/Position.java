package com.mgy.example.common.excel;

/**
 * 内容位置
 *
 * @author maguoyong
 * @since 2018/01/24
 */
public interface Position {
    short GENERAL = 0;
    short LEFT = 1;
    short CENTER = 2;
    short RIGHT = 3;
    short FILL = 4;
    short JUSTIFY = 5;
    short CENTER_SELECTION = 6;
    short DISTRIBUTED = 7;
}
