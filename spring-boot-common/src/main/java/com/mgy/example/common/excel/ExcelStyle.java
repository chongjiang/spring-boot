package com.mgy.example.common.excel;

/**
 * 表格样式
 *
 * @author maguoyong
 * @since 2018/01/24
 */
public class ExcelStyle {

    public Short getBgColor() {
        return bgColor;
    }

    public void setBgColor(Short bgColor) {
        this.bgColor = bgColor;
    }

    public Short getFontColor() {
        return fontColor;
    }

    public void setFontColor(Short fontColor) {
        this.fontColor = fontColor;
    }

    public Short getFontSize() {
        return fontSize;
    }

    public void setFontSize(Short fontSize) {
        this.fontSize = fontSize;
    }

    public Short getFontWeight() {
        return fontWeight;
    }

    public void setFontWeight(Short fontWeight) {
        this.fontWeight = fontWeight;
    }

    public Short getBorderStyle() {
        return borderStyle;
    }

    public void setBorderStyle(Short borderStyle) {
        this.borderStyle = borderStyle;
    }

    public Short getPosition() {
        return position;
    }

    public void setPosition(Short position) {
        this.position = position;
    }

    public Short getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Short borderColor) {
        this.borderColor = borderColor;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }
    public Short getHeight() {
        return height;
    }

    public void setHeight(Short height) {
        this.height = height;
    }
    /**
     * 行背景颜色
     */
    private Short bgColor;

    /**
     * 字体颜色
     */
    private Short fontColor;
    /**
     * 字体大小
     */
    private Short fontSize;
    /**
     * 字体加粗
     */
    private Short fontWeight;

    /**
     * 单元格内容居中或者其他
     */
    private Short position;

    /**
     * 单元格边框样式
     */
    private Short borderStyle;
    /**
     * 边框颜色
     */
    private Short borderColor;

    /**
     * 字体名称
     */
    private String fontName;

    /**
     * 行高
     */
    private Short height;
}
