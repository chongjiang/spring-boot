package com.mgy.example.common.excel;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 行定义
 *
 * @author maguoyong
 * @since 2018/01/24
 */
public class DataRow {
    /**
     * 定义该行记录在table所处的行数
     */
    private int rowIndex = -1;
    private DataColumnCollection columns;

    private DataTable table;

    /**
     * 用于存储数据的Map对象，这里保存的对象不包括顺序信息，数据获取的索引通过行信息标识
     */
    private Map<String, Object> itemMap = new LinkedHashMap<String, Object>();

    public DataRow() {

    }

    public DataRow(DataTable table) {
        this.table = table;
    }

    /**
     * 获取当前行的行索引
     */
    public int getRowIndex() {
        return rowIndex;
    }

    /**
     * 功能描述： 获取当前行所属数据表对象
     */
    public DataTable getTable() {
        return this.table;
    }


    public void setColumns(DataColumnCollection columns) {
        this.columns = columns;
    }

    public DataColumnCollection getColumns() {
        return columns;
    }

    public void setValue(int index, Object value) {
        setValue(this.columns.get(index), value);
    }

    public void setValue(String columnName, Object value) {
        setValue(this.columns.get(columnName), value);
    }

    public void setValue(DataColumn column, Object value) {
        if (column != null) {
            getItemMap().put(column.getColumnName(), column.convertTo(value));
        }
    }

    public void setValues(Object... values) {
        for (int i = 0; i < values.length; i++) {
            setValue(i, values[i]);
        }
    }

    public Object getValue(int index) {
        String colName = this.columns.get(index).getColumnName();
        return this.getItemMap().get(colName);
    }

    public Object getValue(String columnName) {
        return this.getItemMap().get(columnName);
    }

    public Map<String, Object> getItemMap() {
        return itemMap;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }


    public boolean getMerge() {
        return this.merge;
    }

    public void setMerge(boolean merge) {
        this.merge = merge;
    }

    /**
     * 是否合并行
     */
    private boolean merge = false;
}
