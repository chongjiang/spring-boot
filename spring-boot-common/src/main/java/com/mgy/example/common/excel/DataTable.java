package com.mgy.example.common.excel;

import java.util.ArrayList;
import java.util.List;

/**
 * 表格
 *
 * @author maguoyong
 * @since 2018/01/24
 */
public final class DataTable {
    /**
     * 用于保存DataRow的集合对象
     */
    private DataRowCollection rows;
    /**
     * 用于保存DataColumn的对象
     */
    private DataColumnCollection columns;
    /**
     * 表名
     */
    private String tableName = "tableName";

    /**
     * 报表头样式
     */
    private ExcelStyle headerStyle;

    /**
     * 列头样式
     */
    private ExcelStyle columnStyle;

    /**
     * 内容样式
     */
    private ExcelStyle contentStyle;

    /**
     * 表头内容
     */
    private String header;

    /**
     * 合并单元格
     */
    private List<CellRange> cellRanges;


    public DataTable() {
        this.columns = new DataColumnCollection(this);
        this.rows = new DataRowCollection(this);
        this.rows.setColumns(columns);
        this.headerStyle = new ExcelStyle();
        this.columnStyle = new ExcelStyle();
        this.contentStyle = new ExcelStyle();
        this.cellRanges=new ArrayList<>();
    }

    public DataTable(String dataTableName) {
        this();
        this.tableName = dataTableName;
    }

    /**
     * 列头样式
     *
     * @return
     */
    public ExcelStyle getColumnStyle() {
        return columnStyle;
    }


    /**
     * 内容样式
     *
     * @return
     */
    public ExcelStyle getContentStyle() {
        return contentStyle;
    }

    public ExcelStyle getHeaderStyle() {
        return headerStyle;
    }


    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<CellRange> getCellRanges() {
        return cellRanges;
    }

    public void setCellRanges(List<CellRange> cellRanges) {
        this.cellRanges = cellRanges;
    }

    public int getTotalCount() {
        return rows.size();
    }


    /**
     * 功能描述： 返回表名
     */
    public String getTableName() {
        return this.tableName;
    }

    /**
     * 功能描述： 设置表名
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * 返回该表引用的封装类
     */
    public DataRowCollection getRows() {
        return this.rows;
    }

    public DataColumnCollection getColumns() {
        return this.columns;
    }

    /**
     * 获取指定行指定列的数据
     */

    public Object getValue(int row, String colName) {
        return this.rows.get(row).getValue(colName);
    }

    public Object getValue(int row, int col) {
        return this.rows.get(row).getValue(col);
    }

    /**
     * 为该表数据新建一行
     */
    public DataRow newRow()  {
        DataRow tempRow = new DataRow(this);
        int lastRowIndex;
        if (this.rows.size() > 0) {
            lastRowIndex = this.rows.get(this.rows.size() - 1).getRowIndex();
        } else {
            lastRowIndex = 0;
        }
        tempRow.setColumns(this.columns);
        tempRow.setRowIndex(++lastRowIndex);
        return tempRow;
    }

    public void setValue(int row, int col, Object value) {
        this.rows.get(row).setValue(col, value);
    }

    public void setValue(int row, String colName, Object value) {
        this.rows.get(row).setValue(colName, value);
    }


    public DataColumn addColumn(String columnName, Class dataType) throws Exception {
        return this.columns.addColumn(columnName, dataType);
    }

    public DataColumn addColumnIndex(int index, String columnName, Class dataType) throws Exception {
        return this.columns.addColumn(index, columnName, dataType);

    }

    public boolean addRow(DataRow row)  {
        if (this.rows.size() > 0) {
            row.setRowIndex(this.rows.get(this.rows.size() - 1).getRowIndex() + 1);
        } else {
            row.setRowIndex(1);
        }
        return this.rows.add(row);
    }

    /**
     * 添加一行记录
     *
     * @param values
     */
    public void addRow(Object... values)  {
        DataRow row = this.newRow();
        for (int i = 0; i < values.length; i++) {
            row.setValue(i, values[i]);
        }
        this.rows.add(row);
    }


    public DataTable cloneTable() {
        try {
            DataTable table = new DataTable();
            table.setTableName(this.getTableName());
            for (DataColumn dc : this.columns) {
                table.addColumn(dc.getColumnName(), dc.getDataType());
            }
            return table;
        } catch (Exception ex) {
            return null;
        }
    }

}
