package com.mgy.example.common.excel;

/**
 * @author maguoyong
 * @since 2018/06/27
 */
public class CellRange {
    public int getFirstRow() {
        return firstRow;
    }

    public void setFirstRow(int firstRow) {
        this.firstRow = firstRow;
    }

    public int getFirstColumn() {
        return firstColumn;
    }

    public void setFirstColumn(int firstColumn) {
        this.firstColumn = firstColumn;
    }

    public int getLastRow() {
        return lastRow;
    }

    public void setLastRow(int lastRow) {
        this.lastRow = lastRow;
    }

    public int getLastColumn() {
        return lastColumn;
    }

    public void setLastColumn(int lastColumn) {
        this.lastColumn = lastColumn;
    }

    /**
     * 第一行
     */
    private int firstRow;
    /**
     * 第一列
     */
    private int firstColumn;
    /**
     * 最后一行
     */
    private int lastRow;
    /**
     * 最后一列
     */
    private int lastColumn;

    public CellRange(int firstRow, int firstColumn, int lastRow, int lastColumn) {
        this.firstRow = firstRow;
        this.firstColumn = firstColumn;
        this.lastRow = lastRow;
        this.lastColumn = lastColumn;
    }
}
