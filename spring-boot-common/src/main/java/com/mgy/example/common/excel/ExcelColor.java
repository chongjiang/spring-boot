package com.mgy.example.common.excel;

/**
 * 单元格颜色
 *
 * @author maguoyong
 * @since 2018/01/24
 */
public interface ExcelColor {
    short BLACK = 8;
    short BROWN = 60;
    short OLIVE_GREEN = 59;
    short DARK_GREEN = 58;
    short DARK_TEAL = 56;
    short DARK_BLUE = 18;
    short INDIGO = 62;
    short GREY_80_PERCENT = 63;
    short DARK_RED = 16;
    short ORANGE = 53;
    short DARK_YELLOW = 19;
    short GREEN = 17;
    short TEAL = 21;
    short BLUE = 12;
    short BLUE_GREY = 54;
    short GREY_50_PERCENT = 23;
    short RED = 10;
    short LIGHT_ORANGE = 52;
    short LIME = 50;
    short SEA_GREEN = 57;
    short AQUA = 49;
    short LIGHT_BLUE = 48;
    short VIOLET = 20;
    short GREY_40_PERCENT = 55;
    short PINK = 14;
    short GOLD = 51;
    short YELLOW = 13;
    short BRIGHT_GREEN = 11;
    short TURQUOISE = 15;
    short SKY_BLUE = 40;
    short PLUM = 61;
    short GREY_25_PERCENT = 22;
    short ROSE = 45;
    short TAN = 47;
    short LIGHT_YELLOW = 43;
    short LIGHT_GREEN = 42;
    short LIGHT_TURQUOISE = 41;
    short PALE_BLUE = 44;
    short LAVENDER = 46;
    short WHITE = 9;
    short CORNFLOWER_BLUE = 24;
    short LEMON_CHIFFON = 26;
    short MAROON = 25;
    short ORCHID = 28;
    short CORAL = 29;
    short ROYAL_BLUE = 30;
    short LIGHT_CORNFLOWER_BLUE = 31;
    short AUTOMATIC = 64;
}
