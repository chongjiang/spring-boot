package com.mgy.example.utils;

import com.mgy.example.domain.TableRouter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class DbUtil {
    /**
     * 分库号名称
     */
    public static final String DB_NO = "dbNo";

    /**
     * 分表号名称
     */
    public static final String TABLE_NO = "tableNo";

    /**
     * 逻辑表
     */
    public static final String LOGIC_TABLE = "logicTable";

    /**
     * 获取分库号
     */
    public static Integer getDbNo(String[] paramNames, Class[] paramTypes, Object[] paramValues) throws IllegalAccessException {
        return ParameterUtil.resolve(paramNames, paramTypes, paramValues, DB_NO);
    }

    /**
     * 获取分表号
     */
    public static Integer getTableNo(String[] paramNames, Class[] paramTypes, Object[] paramValues) throws IllegalAccessException {
        return ParameterUtil.resolve(paramNames, paramTypes, paramValues, TABLE_NO);
    }

    /**
     * 获取逻辑表名
     */
    public static String getLogicTable(String[] paramNames, Class[] paramTypes, Object[] paramValues) throws IllegalAccessException {
        return ParameterUtil.resolve(paramNames, paramTypes, paramValues, LOGIC_TABLE);
    }

    /**
     * 获取分表路由
     */
    public static List<TableRouter> getTableRouter(String[] paramNames, Class[] paramTypes, Object[] paramValues) {
        List<TableRouter> tableRouters = new ArrayList<>();
        if (paramNames == null || paramTypes == null || paramValues == null) {
            return null;
        }

        TableRouter tableRouter = null;
        for (int i = 0; i < paramNames.length; i++) {
            String name = paramNames[i];
            Class type = paramTypes[i];
            Object obj = paramValues[i];
            if (BeanUtils.isSimpleValueType(type) && StringUtils.equals(name, DB_NO)) {
                if (tableRouter == null) {
                    tableRouter = new TableRouter();
                }
                tableRouter.setDbNo((Integer) obj);
            } else if (BeanUtils.isSimpleValueType(type) && StringUtils.equals(name, TABLE_NO)) {
                if (tableRouter == null) {
                    tableRouter = new TableRouter();
                }
                tableRouter.setTableNo((Integer) obj);
            } else if (BeanUtils.isSimpleValueType(type) && StringUtils.equals(name, LOGIC_TABLE)) {
                if (tableRouter == null) {
                    tableRouter = new TableRouter();
                }
                tableRouter.setLogicTable((String) obj);
            } else if (TableRouter.class.isAssignableFrom(type) && obj != null) {
                tableRouters.add((TableRouter) obj);
            } else if (obj instanceof Collection) {
                Collection collection = (Collection) obj;
                Iterator iterator = collection.iterator();
                while (iterator.hasNext()) {
                    Object itemObj = iterator.next();
                    if (itemObj != null && TableRouter.class.isAssignableFrom(itemObj.getClass())) {
                        tableRouters.add((TableRouter) itemObj);
                    }
                }
            }
        }
        if (tableRouter != null) {
            tableRouters.add(tableRouter);
        }
        return tableRouters;
    }

}
